const {
  ProcessRoutes,
  GroupRoutes,
  RuleRoutes,
  GenericRoutes,
  UserRoutes,
  TokenRoutes,
  Swagger, PostRoutes, ComputerRoutes
} = require("../src/routes");

module.exports = (app, passport) => {
  
  app.use('/api/v1/user', UserRoutes(passport))
  
  app.use("/api/v1/group", GroupRoutes);

  app.use("/api/v1/rule", RuleRoutes);

  app.use("/api/v1/process", ProcessRoutes(passport))
  
  app.use("/api/v1/computer", ComputerRoutes(passport))

  app.use("/api/v1/token", TokenRoutes);


  app.use("/api/v1/", GenericRoutes)
  
  app.use('/api-docs', Swagger)

}