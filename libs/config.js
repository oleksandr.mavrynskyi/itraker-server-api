const {none} = require("ramda");
config = {}
config.mode=process.env.NODE_ENV || 'development'
config.hostUrl=process.env.HOST_URL || 'localhost'
config.clientUrl=process.env.CLIEND_URL || 'localhost'
config.port=process.env.PORT || 8000
config.mongoose = {
    url: process.env.DB_MONGO_URI || '',
    options: {
        keepAlive: 1,
        useNewUrlParser: true,
        useUnifiedTopology: true,
    },
}
config.templater = {
    engine: process.env.SERVER_VIEW_ENGINE || "ejs",
    path: {
        view: process.env.SERVER_FOLDER_VIEWS || "src/templates/views",
        static: process.env.SERVER_FOLDER_STATIC || "src/templates/static"
    }
}
config.session = {
    secret: process.env.APP_SECRET || "workshopsolt",
    key: "sid",
    cookie: {
        path: '/',
        httpOnly: true,
        secure: true,
        maxAge:60000,
        sameSite: "none",
    }
}

config.authenticate = {
    jwt: {
        accessSecret: process.env.ACCESS_SECRET,
        refreshSecret: process.env.REFRESH_SECRET,
        accessTime: process.env.JWT_ACCESS_TIME || "30m",
        refreshTime: process.env.JWT_REFRESH_TIME || "30d",
    }
}
config.mailer = {
    host: process.env.SMTP_HOST || "smtp.gmail.com",
    port: process.env.SMTP_PORT || 587,
    user: process.env.SMTP_USER,
    password: process.env.SMPT_PASSWORD,
}

config.statusType = {
    enabled: 1,
    disabled: 2,
    nonEnabled: 0,

}

module.exports=config