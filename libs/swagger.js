const path = require("path")
const url = require("url")
const swaggerAutogen = require("swagger-autogen")
const {__} = require("ramda");

module.exports = async ()=> {

const doc = {
    info: {
        version: require("../package.json").version,
        title: 'AlternativeWorld WORKSHOP |platform|',
        description: 'REST-API application wrapper for resistance over links'
    },
    definitions: {},
    host: `${process.env.SERVER_URL}`,
    schemes: ['http']
}

const outputFile = path.join(__dirname,'../src/data/swagger.json')
const endpointsFiles = [path.join(__dirname, '../app.js')]

swaggerAutogen(/*options*/)(outputFile, endpointsFiles, doc).then(({ success }) => {
    console.log(`Generated: ${success}`)
})

}