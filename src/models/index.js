module.exports = {
    UserModel: require('./user.model'),
    GroupModel: require('./group.model'),
    RuleModel: require('./rule.model'),
    TokenModel: require('./token.model'),
    ProcessModel: require('./process.model'),
    ComputerModel: require('./computer.model'),
}