const {model, Schema, Schema: {Types: {ObjectId}}} = require("mongoose")
const {process, computer} = require('./models')
const {Timestamp} = require("mongodb");

const schema = new Schema({
    computer: {
      type: ObjectId,
      ref: computer, 
      required: true,  
    },
    processUUID: {
        type: Number,
        required: true,
    },
    programName: {
        type: String,
        required: true,
    },
    processName: {
        type: String,
    },
    processStartTime: {
        type: Date,
    },
    processEndTime: {
        type: Date,
    },
    processExecutionTime: {
        type: Date,
    },
    processPath: {
        type: String,
        required: true,
    },
    processHandle:{
        type: Number,
        required: true,
    },
});

module.exports = model(process, schema);