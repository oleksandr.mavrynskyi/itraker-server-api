const {Schema, model, Schema: { Types: { ObjectId }} } = require("mongoose");
const {computer, user} = require("./models");
const schema = new Schema({
    name: {
      type: String,
    },
    owner: {
        type: ObjectId,
        ref: user, 
        required: true,
    },
    computerToken: {
        type: String,
        required: true,
        unique: true,
    },
    processes: [{
        type: ObjectId,
        ref: process,
    }],
});

module.exports = model(computer, schema);