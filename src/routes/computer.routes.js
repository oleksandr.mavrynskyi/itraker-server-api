const router = require('express-promise-router')()

const { ComputerController} = require('../controllers')

module.exports = (passport) => {
    router.route('/').post(passport.authenticate('jwt', { session: false }), ComputerController.createPc)
    router.route('/local/:pcToken').put(passport.authenticate('jwt', { session: false }),ComputerController.updatePcLocal)
    router.route('/:pcToken').put(passport.authenticate('jwt', { session: false }),ComputerController.updateByPc)
    router.route('/:id').delete(passport.authenticate('jwt', { session: false }),ComputerController.delete)
    return router
}

