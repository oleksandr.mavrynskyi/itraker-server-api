module.exports = {
    UserRoutes: require('./user.routes'),
    GroupRoutes: require('./group.routes'),
    RuleRoutes: require('./rule.routes'),
    ProcessRoutes: require('./process.routes'),
    ComputerRoutes: require('./computer.routes'),
    GenericRoutes: require('./generic.routes'),
    TokenRoutes: require('./token.routes'),
    Swagger: require('./swagger.routes'),
}