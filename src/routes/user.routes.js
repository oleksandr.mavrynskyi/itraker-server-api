const router = require('express-promise-router')()
const {check} = require("express-validator")
const { UserController, TokenController } = require('../controllers')
const refreshPasswordValidator = require('../validatiors/refreshPassword.validator')
const registrationValidator = require('../validatiors/registration.validator')

module.exports = (passport) => {
    router.route('/registration').post(registrationValidator, UserController.registration)
    router.route('/authorization').post(UserController.authorization)
    router.route('/activate/:link').get(UserController.activate)
    router.route('/logout').post(UserController.logout)
    router.route('/password/refresh').post(refreshPasswordValidator, passport.authenticate('jwt', { session: false }), UserController.refreshPassword)
    
    // ...AUTH TOKEN
    router.route('/token/refresh').get(UserController.refreshToken)
    
    return router
}