const router = require('express-promise-router')()

const { ProcessController} = require('../controllers')

module.exports = (passport) => {
    router.route('/:pcToken').get(passport.authenticate('jwt', { session: false }), ProcessController.getByPc)
    router.route('/').post(passport.authenticate('jwt', { session: false }), ProcessController.sendProcessList)
    router.route('/:pcToken/q').get(passport.authenticate('jwt', { session: false }), ProcessController.queryArgsByPc)
    return router
}

