module.exports = {
    MailService: require('./mail.service'),
    UserService: require('./user.service'),
    GroupService: require('./group.service'),
    CrudService: require('./crud.service'),
    TokenService: require('./token.service'),
    RuleService: require('./rule.service'),
    ProcessService: require('./process.service'),
    ComputerService: require('./computer.service'),
}