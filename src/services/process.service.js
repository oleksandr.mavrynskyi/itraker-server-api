const { ProcessCrud } = require('./cruds')
const boom = require("boom");
const {ComputerModel, ProcessModel} = require("../models");
module.exports = {
    ...ProcessCrud,
    async getByComputer(pcToken) {
        let computer = await ComputerModel.findOne({computerToken: pcToken})
        if (!computer) throw boom.badRequest("computer with that token not found")
        return ProcessModel.find({computerToken: computer._id})
    }
}