const boom = require("boom");
const { UserModel, RuleModel, GroupModel } = require('../models')

const { UserCrud, GroupCrud } = require('./cruds')
const MailService = require('./mail.service')
const TokenService = require('./token.service')
const {UserDto, TestDto} = require('../dtos')
const {hostUrl, statusType} = require("../../libs/config");
const uuid = require("uuid");

module.exports = {
    ...UserCrud,
    async registration(email,password) {
        const candidate = await UserModel.findOne({email: email})
        if (candidate) throw boom.notAcceptable('email is already registered', email)
        const activationCode = uuid.v4()
        const newUser = new UserModel({
            email: email,
            activationLink: activationCode,
        })
        newUser.setPassword(password)
        await newUser.save()
        const url = `${hostUrl}/api/v1/user/activate/${activationCode}`
        await MailService.sendActivationLink(email, url)
        return {url}
    },
    async activate(activationLink) {
        const user = await UserModel.findOne({activationLink:activationLink})
        if(!user) throw boom.badData('invalid activation link',activationLink)
        if(user.status===statusType.enabled)
            throw boom.badData(`user is already activated`)
        user.status = statusType.enabled
        user.activationLink = undefined;
        user.save()
    },
    async authorization(email,password) {
        const user = await UserModel.findOne({email: email})
        if(!user) throw boom.notAcceptable('no user with that email found')
        if(!user.validatePassword(password)) throw boom.notAcceptable('Invalid password')
        const userDto = await new UserDto(user)
        const tokens = await TokenService.generateTokens({...userDto})
        await TokenService.saveToken(userDto.id, tokens.refreshToken)
        return {user: userDto, ...tokens}
    },
    async logout(refreshToken) {
        return await TokenService.removeToken(refreshToken)
    },
    async refreshToken(refreshToken) {
        if(!refreshToken) throw boom.unauthorized()
        const userData = await TokenService.validateRefreshToken(refreshToken)
        const tokenData = await TokenService.getByRefreshToken(refreshToken)
        if(!userData||!tokenData) throw boom.unauthorized()

        let newUser = await UserModel.findById(userData.id)
        const userDto = await new UserDto(newUser)
        const tokens = await TokenService.generateTokens({...userDto})

        await TokenService.saveToken(userDto.id, tokens.refreshToken)
        return {user:userDto, ...tokens}
    },
}