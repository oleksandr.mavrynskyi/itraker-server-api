const base = require('./crud.service')
const {UserModel, GroupModel, TokenModel, ProcessModel, ComputerModel} = require("../models");
module.exports = {
    UserCrud: base(UserModel),
    GroupCrud: base(GroupModel),
    TokenCrud: base(TokenModel),
    ProcessCrud: base(ProcessModel),
    ComputerCrud: base(ComputerModel),
}