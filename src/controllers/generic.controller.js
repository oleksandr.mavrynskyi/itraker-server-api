const boom = require("boom");
const {blAdapter} = require('../helpers/blacklist')
const {CrudService} = require("../services");
const {forEach} = require("ramda");

const blacklist = new blAdapter()

const genericCrud = (model,dto=null) => ({
  async get({ params: { id } }, res) {
    try {
      let item = await CrudService(model).get(id)
      if (dto!=null) item = await new dto(item)
      res.status(200).send(item);
    } catch (err) { return res.boom.boomify(err) }
  },
  
  async getAll(req, res) {
    try {
      let items = await CrudService(model).getAll()
      if (dto!=null) items = await Promise.all(items.map(async item =>item = await new dto(item)))
      res.status(200).send(items);
    } catch (err) {
      return res.boom.boomify(err)
    }
  },
  async create({ body }, res) {
    try {
      let item = await CrudService(model).create(body)
      if (dto!=null) item = await new dto(item)
      res.status(200).send(item);
    } catch (err) {
      return res.boom.boomify(err)
    }
  },
  async update({ params: { id }, body }, res) {
    try {
      let item = await CrudService(model).update(id,body)
      if (dto!=null) item = await new dto(item)
      res.send(200).send(item);
    } catch (err) {
      return res.boom.boomify(err)
    }
  },
  async delete({ params: { id } }, res) {
    try {
      await CrudService(model).delete(id)
      res.status(200).send('OK');
    } catch (err) {
      return res.boom.boomify(err)
    }
  },
  async queryArgs(req,res) {
    const {params, query} = req
    console.log(query)
    try {
      console.log(1)
      if(blacklist.queryChecker(query)) return res.send(boom.notAcceptable('Query permission denied',query))
      console.log(2)
      let items = await CrudService(model).reqByQuery(query);
      console.log(3)
      items = await Promise.all(items.map(async item=>item = await new dto(item)))
      console.log(4)
      res.status(200).send(items);
    } catch (err) {
      return res.boom.boomify(err)
    }
  },
  async bodyArgs({body},res) {
    try {
      if(blacklist.queryChecker(body)) return res.send(boom.notAcceptable('Query permission denied',body))
      let items = await CrudService(model).reqByQuery(body)
      if (dto!=null) items = await Promise.all(items.map(async item => item = await new dto(item)))
      res.status(200).send(items);
    } catch (err) {
      return res.boom.boomify(err)
    }
  },
  async test(req, res) {
    try {
      res.send(req.session)
      res.status(200).send({ page: "generic routed test" });
    } catch (err) {
      return res.boom.boomify(err)
    }
  },
});

module.exports = genericCrud;
