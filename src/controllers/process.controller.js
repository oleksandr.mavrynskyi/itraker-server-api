const genericCrud = require('./generic.controller')

const { ProcessModel, ComputerModel} = require('../models')
const boom = require("boom");
const {user} = require("../models/models");
const {ComputerService, ProcessService, CrudService} = require("../services");
const timeToDate = require("../helpers/timeImplimentation.helper")
const {ProcessDto} = require("../dtos");
const {blAdapter} = require("../helpers/blacklist");
const blacklist = new blAdapter()

module.exports = {
    ...genericCrud(ProcessModel, ProcessDto),
    async getByPc(req,res) {
        let {user} = req
        const {pcToken} = req.params
        user = await user.populate({path:'computers.token', select: 'computerToken'})
        if (!user.computers.filter(item=>item.token.computerToken===pcToken).length) return res.boom.notAcceptable("this is not your computer")
        try {
            const processes = await ProcessService.getByComputer(pcToken)
            return res.status(200).send(processes)
        } catch (err) {
            return res.boom.boomify(err)
        }
        

    },
    async queryArgsByPc(req,res) {
        let {params, query, user} = req
        const {pcToken} = params
        try {
            if(blacklist.queryChecker(query)) return res.boom.notAcceptable('Query permission denied',query)
            user = await user.populate({path:'computers'})
            if (!user.computers.filter(item=>item.computerToken===pcToken).length) return res.boom.notAcceptable("this is not your computer")
            const computer = user.computers.filter(item=>item.computerToken===pcToken)[0]
            if (query.gte!==undefined)
                query = { ...query,
                    processStartTime : {
                        $gte: new Date(query.gte),
                        $lte: new Date(new Date(query.gte).setDate(new Date(query.gte).getDate() + 1))
                    }
                }
            let items = await ProcessService.reqByQuery({computer: computer._id, ...query});
            items = await Promise.all(items.map(async item=>item = await new ProcessDto(item)))
            res.status(200).send(items);
        } catch (err) {
            return res.boom.boomify(err)
        }
    },
    async sendProcessList(req,res) {
        try {
            const {user} = req
            let {pcToken, processes} = req.body
            let computer = await ComputerModel.findOne({computerToken: pcToken})
            if (!computer) {
                computer = await ComputerService.create({owner: user._id, computerToken: pcToken})
                user.computers = [...user.computers, {name: computer.name||"", token: computer}]
                await user.save()
            }
            processes = JSON.parse(processes)
            const processList = await Promise.all(processes.map(async item=>
            {
                let candidate = {
                    computer: computer,
                    processUUID: item.ProcessUUID,
                    programName: item.ProgramName,
                    processName: item.ProcessName,
                    processStartTime: item.Process_StartTime,
                    processEndTime: item.Process_EndTime,
                    processExecutionTime: timeToDate(item.Process_ExcecutionTime),
                    processPath: item.ProcessPath,
                    processHandle: item.ProcessHandle.value
                }
                candidate = await ProcessService.create(candidate)
                await ComputerService.update(computer._id, {processes: [...computer.processes, candidate._id]})
                return candidate
            }))
            return res.status(200).send("OK")
        }
        catch(err) {
            return res.boom.boomify(err)
        }
    }
}