module.exports = {
    UserController: require('./user.controller'),
    GroupController: require('./group.controller'),
    RuleController: require('./rule.controller'),
    GenericController: require('./generic.controller'),
    TokenController: require('./token.controller'),
    MailController: require('./mail.controller'),
    ProcessController: require('./process.controller'),
    ComputerController: require('./computer.controller'),
}