const genericCrud = require('./generic.controller')

const { ComputerModel} = require('../models')
const {ComputerDto} = require("../dtos");
const {ComputerService} = require("../services");
const {computers} = require("../dtos/user.dto");

module.exports = {
    ...genericCrud(ComputerModel, ComputerDto),
    async updateByPc(req,res){
        try {
            let {body,user} = req
            const {pcToken} = req.params
            user = await user.populate({path:'computers'})
            if (!user.computers.filter(item=>item.computerToken===pcToken).length) return res.boom.notAcceptable("this is not your computer")
            const computer = user.computers.filter(item=>item.computerToken===pcToken)[0]
            if (body.name!==undefined) computer.name = body.name
            await computer.save()
            return res.status(200).send(computer)
        }
        catch (err) {
            return res.boom.boomify(err)
        }
    },
    async updatePcLocal(req,res){
        try {
            let {body,user} = req
            const {pcToken} = req.params
            user = await user.populate({path:'computers.token'})
            if (!user.computers.filter(item=>item.token.computerToken===pcToken).length) return res.boom.notAcceptable("this is not your computer")
            let computer
            user.computers.map(item=>{
                if (item.token.computerToken===pcToken) {
                    if (body.name!==undefined) item.name = body.name
                    computer = item
                }
            })
            await user.save()
            return res.status(200).send(computer)
        }
        catch (err) {
            return res.boom.boomify(err)
        }
    },
    async createPc(req, res) {
        const {user,body} = req
        const computer = await ComputerService.getOne({computerToken: body.computerToken})
        console.log(computer)
        if (computer) return  res.boom.badRequest("this token is already exists")
        try {
            const candidate = {
                owner: user._id,
                computerToken: body.computerToken,
                name: body.name||undefined,
            }
            const computer = await ComputerService.create(candidate)
            user.computers = [...user.computers, {token: computer._id, name: body.name||""}]
            await user.save()
            return res.status(200).send(computer)
        } catch (err) {
            return res.boom.boomify(err)
        }
    }
}