const genericCrud = require('./generic.controller')
const passport = require("passport");
const {validationResult} = require('express-validator')
const { UserModel, RuleModel, GroupModel } = require('../models')
const boom = require("boom");
const {UserService, TokenService, ComputerService} = require("../services");
const {UserDto} = require("../dtos");
const {clientUrl} = require("../../libs/config");
const {config} = require("dotenv");
const {user} = require("../models/models");

module.exports = {
    ... genericCrud(UserModel, UserDto),
    async registration(req , res) {
        try {
            const validationErrors = validationResult(req)
            if(!validationErrors.isEmpty())
                return res.status(422).send(boom.badData(validationErrors))
            const {email,password} = req.body
            const link = await UserService.registration(email,password)
            return res.status(200).send({data: link});
        } catch (err) {
            return res.boom.boomify(err)
        }
    },
    async authorization(req,res) {
        try{
            const {email, password} = req.body;
            const userData = await UserService.authorization(email,password)
            res.cookie('refreshToken',userData.refreshToken, {httpOnly:true})
            return res.status(200).send(userData);
        } catch(err) {
            return res.boom.boomify(err)
        }
    },
    async activate({params: {link}},res) {
        try{
            await UserService.activate(link)
            return  res.status(200).send("OK")
        } catch (err) {
            return res.boom.boomify(err)
        }
    },
    async logout({cookies},res) {
        try{
            
            const {refreshToken} = cookies
            const token = await UserService.logout(refreshToken)
            res.clearCookie('refreshToken')
            return res.status(200).send(token)
        } catch (err) {
            return res.boom.boomify(err)
        }
    },
    async refreshToken({cookies},res) {
        try{
            const {refreshToken} = cookies
            const userData = await UserService.refreshToken(refreshToken)
            res.cookie('refreshToken',userData.refreshToken, {httpOnly:true})
            return res.status(200).send(userData);
        } catch (err) {
            return res.boom.boomify(err)
        }
    },
    async refreshPassword(req,res) {
        try {
            const {user} = req
            const {password, newPassword} = req.body
            if (user.validatePassword(password)) return res.boom.BadRequest("wrong password")
            user.setPassword(newPassword)
            await user.save()
            return res.status(200).send("OK")
        }
        catch(err) {
            return res.boom.boomify(err)
        }
    }
}