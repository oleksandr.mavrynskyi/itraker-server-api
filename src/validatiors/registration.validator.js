const {generateValidations} = require('./generic')

const registrationOptions = {
    email: {
        isEmail: true,
        required: true
    },
    password: {
        length: {
            min: 8,
            max: 32,
        },
        required: true
    },
}

module.exports=generateValidations(registrationOptions)