const {generateValidations} = require("./generic");
const refreshPasswordOptions = {
    password: {
        length: {
            min: 8,
            max: 32,
        },
        required: true
    },
}

module.exports=generateValidations(refreshPasswordOptions)