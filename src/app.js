'use strict'

require("dotenv").config()

// INJECTS ....................................
const express                         = require("express")
const path                              = require("path")
const logger               = require("morgan")
const cookieParser         = require("cookie-parser")
const cors                = require("cors")
const bodyParser                    = require("body-parser")
const passport                          = require("passport")
const session             = require('express-session')
const boom = require('express-boom');const router          = require("../libs/router")
const config          = require('../libs/config')
const {createServer} = require("http");

const app = express()
app.use((req,res,next)=>{
  res.header("Access-Control-Allow-Origin", `${req.header("Origin")}`); // update to match the domain you will make the request from
  res.header("Access-Control-Allow-Credentials", "true");
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, PATCH, DELETE, OPTIONS, HEAD")
  res.header("Access-Control-Allow-Headers", "Authorization, Origin, Referer, Connection, X-Requested-With, Content-Type, Accept");
  next();
})

// CORS ...................................
//app.options("*", cors())


//app.set('trust proxy', 1);

// EXPRESSIONS ...............................
function normalizePort(val) {
  const port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}
app.use(boom());


// COMPONENTS .................................
app
  .use(logger("dev"))
  .use(bodyParser.json())
  .use(express.urlencoded({ extended: true }))
  .use(cookieParser())

// SESSION ...................................
//const sessionStore = require('./libs/session')
app.use(session({
  secret: config.session.secret,
  key: config.session.key,
  cookie: config.session.cookie,
  //store: sessionStore,
  resave:false,
  saveUninitialized: true,
}))

// PASSPORT ..................................
app.use(passport.initialize())
app.use(passport.session())

require('../libs/passport')(passport)
// TEMPLATES ..................................
app
  .set("view engine", config.templater.engine)
  .set("view", path.join(__dirname, config.templater.path.view))
  .use(express.static(path.resolve(__dirname, config.templater.path.static)));

// MIDDLEWARES ................................

// OPENAPI ....................................
//require('./libs/swagger')()

/* 4 04 HANDLER */
//app.use(function(req, res, next) {
//  res.status(404);
//  
//  if (req.accepts('html')) {
//    res.render('404', { url: req.url });
//    return;
//  }
//
//  if (req.accepts('json')) {
//    res.json({ error: 'Not found' });
//    return;
//  }
//
//  res.type('txt').send('Not found');
//});


// ROUTER ....................................
require('../libs/router')(app,passport)

// .............................................

const PORT = normalizePort(process.env.PORT || config.port)

// DATABSE
  require('../libs/mongoose')

async function start() {
    // LISTENER ................................
    createServer({}, app).listen(PORT)
    console.log(`${config.mode}:\tServer listen from  ${config.hostUrl}:${PORT}`)
}

start()