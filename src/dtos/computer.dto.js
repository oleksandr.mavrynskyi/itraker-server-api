module.exports = function UserDto(model, args = {}) {
    return (async ()=> {
        
        this.owner = model.owner
        this.computerToken = model.computerToken
        this.name = model.name
        return this
    })()
}
