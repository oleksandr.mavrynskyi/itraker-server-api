module.exports = function ProcessDto(model, args = {}) {
    return (async ()=> {

        this.computerToken = model.computerToken
        this.processUUID = model.processUUID
        this.programName = model.programName
        this.processName = model.processName
        this.processStartTime = model.processStartTime
        this.processEndTime = model.processEndTime
        this.processExecutionTime = model.processExecutionTime
        this.processPath = model.processPath
        this.processHandle = model.processHandle
        return this
    })()
}
