const {statusType} = require("../../libs/config");
const UserDto = require("./user.dto");
module.exports = function GroupDto(model, args={}) {
    return (async ()=>{

        this.id=model._id
        this.groupname=model.groupname
        this.owner=model.owner
        
        if (model.status === statusType.disabled||args.reasonDescription!==undefined)
            this.reasonDescription = model.reasonDescription
        this.created=model.created
        
        return this
    })()
    
    //if (args.users!==undefined) this.users = model.populate({path:'users'}).users.map(user =>new UserDto(user))
    //    else this.users = model.populate({path:'users', select:'username'}).groups
    //if (args.rules!==undefined) this.rules = model.populate({path:'rules'}).rules.map(rule=>rule)
    //    else this.rules = model.populate({path:'rules',select:'rulename'}).rules
    //this.status=model.status
}