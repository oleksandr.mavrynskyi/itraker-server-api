module.exports = {
    UserDto: require('./user.dto'),
    ProcessDto: require('./process.dto'),
    ComputerDto: require('./computer.dto'),
    GroupDto: require('./group.dto'),
    TestDto: require('./test.dto'),
}