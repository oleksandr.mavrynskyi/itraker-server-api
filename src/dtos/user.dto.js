const {statusType} = require("../../libs/config");
const GroupDto = require("./group.dto");
const {ComputerDto} = require("./computer.dto");
module.exports = function UserDto(model, args = {}) {
    return (async ()=>{
        let tempItemMap
        
        this.id = model._id
        this.email = model.email
        this.rules = model.rules
        // GROUPS CONFIGURATION
        if (args.groups!==undefined) {
            await model.populate({path:'groups'})
                .then(({groups}) => tempItemMap = groups)
            this.groups = await Promise.all(tempItemMap.map(async item=>await new GroupDto(item, args.groups)))
        }
        else await model.populate({path: 'groups', select:'groupname'}).then(({groups})=>this.groups=groups)
        
        if (args.computers!==undefined) {
            await model.populate({path:'computers'})
                .then(({computers}) => tempItemMap = computers)
            this.computers = await Promise.all(tempItemMap.map(async item=>await new ComputerDto(item, args.computers)))
        }
        else await model
            .populate({path: 'computers.token', select:'computerToken name'})
            .then(({computers})=>this.computers=computers)
        
        this.status = model.status
        if (model.status === statusType.disabled || args.reasonDescription !== undefined)
            this.reasonDescription = model.reasonDescription
        this.created = model.created
        return this
    })()
}
